const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    baseUrl: 'https://localhost:8443',
  },
  env: {
    "testUsername": "admin",
    "testPassword": ""
  }
});
