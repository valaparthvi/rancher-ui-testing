## Prerequisite
1. Install Rancher UI
```sh
docker run --name rancher-ui --privileged -d --restart=unless-stopped -p 8080:80 -p 8443:443 rancher/rancher
```
2. Follow the instruction to login.

3. Set the password for 'admin' in `cypress.config.js`. If you intend to use a different username:password, make sure it is updated in the file.

## Running the Tests
1. Run Rancher UI using docker. The `cypress.config.js` has already been configured to use `https://localhost:8443`. If you use a different mode of installation for Rancher UI, change the `.e2e.baseURL` with the new URL.
2. Ensure you add username and password to `.env.testUsername`, and `.env.testPassword` in `cypress.config.js`.
3. `./node-modules/.bin/cypress open`, click on 'E2E Testing', select a browser of your choosing, and click on `Start E2E Testing in <BrowserName>`. Run `login_ui_testing.cy.js` test.

## Cleanup
1. Stop the Docker container.
```sh
docker stop rancher-ui
```
2. Remove the container.
```sh
docker rm rancher-ui
```