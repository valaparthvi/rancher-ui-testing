/// <reference types="cypress" />

describe('rancher UI login', () => {

  beforeEach(() => {
    cy.visit('/')
    expect(Cypress.env('testUsername')).not.eq('')
    expect(Cypress.env('testPassword')).not.eq('')
  })

  it('should let user log in', () => {
    // Log in to the UI
    cy.get('input[id="username"]').type(Cypress.env('testUsername'))
    cy.get('input[type="password"]').type(Cypress.env('testPassword'))
    cy.get('button').contains('Log in with Local User').click()

    // If the user menu exists, we can assume that the web page has opened up
    cy.get('div[class="user user-menu"]').should("exist")

    // Ensure that the title of the page is correct
    cy.title().should("equal", "Rancher")
  })
})